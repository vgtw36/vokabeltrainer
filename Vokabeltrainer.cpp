#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "string.h"
#include "menu.h"
#include "time.h"
using namespace std;

struct vokabel{
	char de[20];
	char eng[20];
	int truecnt = 0;

};

int main(){
	vokabel vok[5], wortspeicher[1];
	char eingabe[20];
	int r, r2, lngeng, wortanzahl;

	wortanzahl = 5;

	srand(time(NULL));

	menu();

	//Vokabeleingabe
	for (int i = 0; i < 5; i++) {
		printf("Gib eine neue englische Vokabel ein: ");
		gets_s(vok[i].eng);
		printf("Gib die deutsche Uebersetzung fuer %s ein: ", vok[i].eng);
		gets_s(vok[i].de);
		printf("\n");
	}
	printf("__________________________________________________\n");
	printf("Starte Uebung\n");
	printf("__________________________________________________\n");

	do {
		//Alle richtig
		if (wortanzahl == 0) {
			printf("__________________________________________________\n");
			printf("Du hast alle Vokabeln richtig erraten, fang von \n vorne an oder beginne eine neue Liste.\n");
			printf("__________________________________________________\n");
			menu();
			wortanzahl = 5;
			for (int i = 0; i < wortanzahl; i++) {
				vok[i].truecnt = 0;
			}
		}

		//Vokabelabfrage
		r = rand() % wortanzahl;
		printf("Uebersetze %s: ", vok[r].de);

		//Antworteingabe
		gets_s(eingabe);

		if (strcmp(eingabe, vok[r].eng) == 0) {
			vok[r].truecnt++;
			printf("Richtig! Du hast %s schon %imal richtig erraten\n\n", vok[r].eng, vok[r].truecnt);

			//Aussortieren
			if (vok[r].truecnt >= 3) {
				swap(wortspeicher[0].de, vok[r].de);
				swap(wortspeicher[0].eng, vok[r].eng);
				wortspeicher[0].truecnt = vok[r].truecnt;

				swap(vok[r].de, vok[wortanzahl - 1].de);
				swap(vok[r].eng, vok[wortanzahl - 1].eng);
				vok[r].truecnt = vok[wortanzahl - 1].truecnt;

				swap(vok[wortanzahl - 1].de, wortspeicher[0].de);
				swap(vok[wortanzahl - 1].eng, wortspeicher[0].eng);
				vok[wortanzahl - 1].truecnt = wortspeicher[0].truecnt;

				wortanzahl--;
			}
		}

		//Neustart
		else if (strcmp(eingabe, "!neustart") == 0) {
			wortanzahl = 5;
			for (int i = 0; i < wortanzahl; i++) {
				vok[i].truecnt = 0;
			}
		}

		//Neue Liste
		else if (strcmp(eingabe, "!neueliste") == 0) {
		    printf("\n");
			for (int i = 0; i < 5; i++) {
				printf("Gib eine neue englische Vokabel ein: ");
				gets_s(vok[i].eng);
				printf("Gib die deutsche Uebersetzung fuer %s ein: ", vok[i].eng);
				gets_s(vok[i].de);
				printf("\n");
			}
		}

		//Tipp
		else if (strcmp(eingabe, "!tipp") == 0) {
			printf("Tipp fuer die Uebersetzung von %s: ", vok[r].de);
			lngeng = strlen(vok[r].eng);
			r2 = rand() % lngeng;
			for (int i = 0; i < lngeng; i++) {
				if (i == r2) {
					printf("%c", vok[r].eng[i]);
				}
				else {
					printf("_");
				}
			}
			printf("\n\n");
		}

		//Menü
		else if (strcmp(eingabe, "!menu") == 0) {
			menu();
		}

		//Falsche antwort
		else{
			printf("Falsch!Versuchs nochmal!\n\n");
		}

	} while (strcmp(eingabe, "!ende") != 0); //Ende

	return 0;
}